import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// Pages
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Cart from './pages/Cart';
import Profile from './pages/Profile';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import PageNotFound from './pages/PageNotFound';

// Additional
// import Footer from './components/Footer';

// Admin
import AdminNavBar from './components/AdminNavBar';
import AdminOrders from './pages/AdminOrders';
import AdminProducts from './pages/AdminProducts';
import AdminUsers from './pages/AdminUsers';

function App() {

  // User State
  const [user, setUser] = useState({
    id: null,
    firstName: null,
    lastName: null,
    email: null,
    isAdmin: null
  });

  // Logout: Clear the local storage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=> {
    fetch("https://frozen-beach-99494.herokuapp.com/users/profile", {
      headers: {
        Authorization:`Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          isAdmin: data.isAdmin
        })
      }
      
      else {
        setUser({
          id: null,
          firstName: null,
          lastName: null,
          email: null,
          isAdmin: null
        })
      }

    });
}, []);


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <div className="custom-page-bg">
        {
          (user.isAdmin) ? <AdminNavBar/> : <NavBar/>
        }
          <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/all" element={<AdminProducts/>}/>
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/orders" element={<AdminOrders/>}/>
            <Route path="/allUsers" element={<AdminUsers/>}/>
            <Route path="/cart" element={<Cart/>}/>
            <Route path="/profile" element={<Profile/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="*" element={<PageNotFound/>}/>
          </Routes>
        </div>
      </Router>
    </UserProvider>
  );
}

export default App;
