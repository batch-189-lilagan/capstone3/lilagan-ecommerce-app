// React Dependencies
import React from 'react';
import ReactDOM from 'react-dom/client';

// Styles
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// Main App
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
