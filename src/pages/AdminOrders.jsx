import { useState, useEffect } from 'react';
import {Container, Table} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function About() {

    const [orders, setOrders] = useState([]);

    const [reload, setReload] = useState(false);

    useEffect(() => {
        fetch('https://frozen-beach-99494.herokuapp.com/users/orders', {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {

            if (data) {
                setOrders(
                    data.map(order => {
                        return (
                            <tr key={order._id} className={order.isDelivered ? 'deliver-success' : 'bg-light'}>
                                <td>{order._id}</td>
                                <td>{order.email}</td>
                                <td>{order.isDelivered ? 'Delivered' : 'Pending'}</td>
                                <td>{order.total}</td>
                                <td>
                                    {
                                        order.isDelivered
                                            ?   <button className='custom-deliver-disabled' onClick={() => deliverOrder(order._id)} disabled>Deliver</button>
                                            :   <button className='custom-button-primary custom-deliver-button ' onClick={() => deliverOrder(order._id)}>Deliver</button>
                                    }
                                </td>
                            </tr>
                        )
                    })
                )
            }

            else {
                setOrders(<tr><td colSpan={5}>No orders yet</td></tr>)
            }   
        })

    }, [reload])

    const deliverOrder = (id) => {
        fetch(`https://frozen-beach-99494.herokuapp.com/users/${id}/deliver`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {

            if (data) {
                Swal.fire({
                    title: 'Order Delivered',
                    icon: 'success',
                });

                setReload(!reload);
            }

            else {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error',
                });
            }

        })
    }

    return (
        <>
            <div className=''>
                <Container className="custom-account-wrapper py-5">
                    <h1 className='custom-page-title mb-5'>All Orders.</h1>
                    <div className='custom-account-section'>
                        <Table hover>
                            <thead>
                                <tr>
                                    <th>Order Id</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {orders}
                            </tbody>
                        </Table>
                    </div>
                </Container>
            </div>
        </>
    );

};