import {Container} from 'react-bootstrap';

export default function About() {

    return (
        <div className="custom-bg">
            <Container>
                <h1>
                    About
                </h1>
            </Container>
        </div>
    );

};