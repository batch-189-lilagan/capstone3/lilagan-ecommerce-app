import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Container} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';

export default function Products() {

    const {user} = useContext(UserContext);

    const [products, setProducts] = useState([]);
    const [isFetched, setIsFetched] = useState(false);
    const [isSoldOut, setIsSouldOut] = useState(false);

    useEffect(() => {

        fetch('https://frozen-beach-99494.herokuapp.com/products/')
        .then(res => res.json())
        .then(data => {
            if (data === false) {
                setIsSouldOut(true);
                setIsFetched(true)
            }
            
            else {
                setProducts(data.map(product => {
                    return (
                        <ProductCard key={product._id} productData={product}/>
                    )
                }));
                setIsFetched(true);
                setIsSouldOut(false);
            }
        })
    }, [])

    

    return (
        (user.isAdmin)
            ?   <Navigate to="/products/all"/>    
            :    <div className='custom-bg-white'>
                    <Container className="custom-account-wrapper py-5">
                        <h1 className='custom-page-title mb-5'>Products.</h1>
                        <div className='custom-products-section'>
                            <div className='custom-products-wrapper'>
                                {
                                    isFetched
                                        ?   isSoldOut 
                                                ?   <div>
                                                        <h2 className="custom-page-sub-title mb-3">Oops! We're sold out!</h2>
                                                    </div>
                                                :   <>{products}</>
                                        :   <div>
                                                <h2 className="custom-page-sub-title mb-3">Defining perfection...</h2>
                                            </div>
                                }
                            </div>
                        </div>
                    </Container>
                </div>
    );

};