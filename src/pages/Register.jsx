import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Container, Form } from 'react-bootstrap';

export default function Register() {

    // Unpack the User Context here
    const { user } = useContext(UserContext);

    // Use this to redirect us to the login page after registration
    const navigate = useNavigate();


    // State hooks for the inputs
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);


    // useEffect to monitor the input field
    useEffect(() => {
        let isInputNotEmpty = (firstName !== '' && lastName !== '' && password1 !== '' && password2 !== '');
        let isPasswordMatch = password1 === password2;

        if (isInputNotEmpty && isPasswordMatch) {
            setIsActive(true);
        }
        
        else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, password1, password2])


    // Register Button Function
    function registerUser(e) {

        e.preventDefault();

        fetch("https://frozen-beach-99494.herokuapp.com/users/register", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            // If there is a server error
            if (data.error === 'serverError') {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error',
                    text: 'A server error occured when trying to make the request. Please try again.'
                })
            }

            // If invalid inptus
            else if (data.error === 'invalidInputs') {
                Swal.fire({
                    title: 'Invalid Inputs',
                    icon: 'warning',
                    text: 'Please check your inputs. Make sure to enter a valid name.'
                });
            }

            // If email is registered already
            else if (data.error === 'emailExists') {
                Swal.fire({
                    title: 'Email already registered',
                    icon: 'error',
                    text: 'The email you entered is already registered. Try using a different email or log in using the email you entered.'
                });
            }

            // If success, clear the input fields and redirect
            else {

                setFirstName('');
                setLastName('');
                setEmail('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                    title: 'Registered successfully!',
                    icon: 'success',
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 6000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                navigate('/login');
            }

        });
    };


    return (
        // Redirect to Home if user tried to access /register
        (user.id !== null) ? <Navigate to="/"/>
        :
        <div className="custom-register-bg">
                <Container className="custom-login-wrapper">
                    <Form className="custom-form border" onSubmit={(e) => registerUser(e)}>
                        <h2 className="custom-page-title mb-4">Register.</h2>

                        {/* First Name */}
                        <Form.Group className="mb-3" controlId="formFirstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Patrick"
                                className="custom-input"
                                value={firstName}
                                onChange={e => setFirstName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Last Name */}
                        <Form.Group className="mb-3" controlId="formLastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Lilagan"
                                className="custom-input"
                                value={lastName}
                                onChange={e => setLastName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Email Address */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                className="custom-input"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        {/* Password 1 */}
                        <Form.Group className="mb-3" controlId="formBasicPassword1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                className="custom-input"
                                value={password1}
                                onChange={e => setPassword1(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                Minimum of 4 characters.
                            </Form.Text>
                        </Form.Group>

                        {/* Password 2 */}
                        <Form.Group className="mb-3" controlId="formBasicPassword2">
                            <Form.Label>Confirm your Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                className="custom-input"
                                value={password2}
                                onChange={e => setPassword2(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* If inputs contains a value and password matches, enable the button*/}
                        {
                            isActive
                                ?   <button className="custom-button-primary mt-1" type="submit">Register</button>
                                :   <button className="custom-button-disabled mt-1" type="submit">Register</button>
                        }

                    </Form>
                </Container>
            </div>
    );

};