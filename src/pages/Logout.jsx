import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Logout() {

    const { unsetUser, setUser } = useContext(UserContext);

    // Clear the local storage upon log out
    unsetUser();

    useEffect(() => {
		setUser({
            id: null,
            firstName: null,
            lastName: null,
            email: null,
            isAdmin: null
        })
	}, [])

    Swal.fire({
        title: 'Signed out successfully!',
        icon: 'success',
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 5000,
        timerProgressBar: false,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })


    return (
        <Navigate to="/"/>
    );

};