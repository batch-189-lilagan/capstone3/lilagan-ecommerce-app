import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Container } from "react-bootstrap";
import { useParams } from "react-router-dom";

import SingleProduct from '../components/SingleProduct';

export default function ProductView() {

    const { user } = useContext(UserContext);

    const { productId } = useParams();
    const [product, setProduct] = useState(null);

    useEffect(() => {

        fetch(`https://frozen-beach-99494.herokuapp.com/products/${productId}`)
        .then(res => res.json())
        .then(data => {

            if (data !== false) {
                setProduct(
                    // data
                    <SingleProduct key={data._id} productData={data}/> 
                )
            }       

            else {
                setProduct(
                    <h2 className="custom-page-sub-title mb-3">Perfect does exist. But not this product.</h2>
                )
            }
        })

    }, [])


    return (
        <div className='custom-product-bg'>
            <Container className="custom-product-view-wrapper py-5">
                {
                    user.isAdmin
                        ?   <h1 className='custom-page-title mb-5'>Edit Product.</h1>
                        :   <h1 className='custom-page-title mb-5'>Product Overview.</h1>
                }
                {product}
            </Container>
        </div>
    );

};