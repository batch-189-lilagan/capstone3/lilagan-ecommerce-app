import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Container, Button, Modal, Form} from 'react-bootstrap';
import AdminProductCard from '../components/AdminProductCard';
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2';

export default function AdminProducts() {

    const {user} = useContext(UserContext);

    const [products, setProducts] = useState([]);
    const [isFetched, setIsFetched] = useState(false);

    // Add Modal State
    const [showAdd, setShowAdd] = useState(false);

    // Default Values for the add product useStates
    const defaultName = 'PTRCK Tee';
    const defaultDescription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo';
    const defaultPicture = 'https://scontent.fmnl17-1.fna.fbcdn.net/v/t39.30808-6/294958613_1362225577597787_4538317371566605194_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHpvuVy3fyVIunfILnNHs397rTk67sr1sPutOTruyvWwzrq7zs6TDSpj3TGrNmZYmk9xzgNu-K6rlZ4cjxBOmzp&_nc_ohc=TlOINoWNDPQAX-dBIir&_nc_ht=scontent.fmnl17-1.fna&oh=00_AT-eDcSXXJNbFj-i3fCMtVOoPNIzm1nBWNV8Ts3fY-vw3Q&oe=62E2265A';
    const defaultQty = 0;
    const defaultPrice = 0;

    // Add Product Form useStates
    const [productName, setProductName] = useState(defaultName);
    const [productDescription, setProductDescription] = useState(defaultDescription);
    const [productPicture, setProductPicture] = useState(defaultPicture);
    const [productQty, setProductQty] = useState(defaultQty);
    const [productPrice, setProductPrice] = useState(defaultPrice);

    const [isActiveAdd, setIsActiveAdd] = useState(false);

    // 
    const [retrieveProducts, setRetrieveProducts] = useState(false);

    const getAllProducts = () => {
        setRetrieveProducts(!retrieveProducts);
    };


    // useEffect for diplaying products
    useEffect(() => {

        fetch('https://frozen-beach-99494.herokuapp.com/products/all', {
            headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
        })
        .then(res => res.json())
        .then(data => {
            if (data === false) {
                setIsFetched(true)
            }
            
            else {
                setProducts(data.map(product => {
                    return (
                        <AdminProductCard key={product._id} productData={product} getProducts={getAllProducts}/>
                    )
                }));
                setIsFetched(true);
            }
        })

        console.log('loaded');
    }, [showAdd, retrieveProducts]);


    // useEffect for the input and view 
    useEffect(() => {

        if (productName === '') {
            setProductName(defaultName)
        }

        if (productDescription === '') {
            setProductDescription(defaultDescription)
        }

        if (productPicture === '') {
            setProductPicture(defaultPicture)
        }

        if (productQty === '') {
            setProductQty(defaultQty)
        }

        if (productPrice === '') {
            setProductPrice(defaultPrice)
        }

        let validInput =
            productName !== defaultName &&
            productDescription !== defaultDescription &&
            productPrice !== defaultPrice &&
            productQty !== defaultQty &&
            productPicture !== defaultPicture;

        if (validInput) {
            setIsActiveAdd(true)
        }

        else {
            setIsActiveAdd(false)
        }

    }, [productName, productDescription, productPicture, productQty, productPrice])


    // Add product submit function 
    const addProduct = (e) => {

        e.preventDefault();

        fetch("https://frozen-beach-99494.herokuapp.com/products/add", {
            method: "POST",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: productName,
                description: productDescription,
                picture: productPicture,
                price: parseInt(productPrice),
                stocks: parseInt(productQty)
            })
        })
        .then(res => res.json())
        .then(data => {
            
            if (data.error === 'serverError') {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }

            else if (data.error === 'productExists') {
                Swal.fire({
                    title: 'Product Exists',
                    icon: 'error',
                    text: 'The product you were adding is already existing in our database. Try again.'
                });
            }

            else {

                Swal.fire({
                    title: 'New Product Added',
                    icon: 'success',
                    text: 'Users will now be able to avail this new perfection.'
                });

                // useState resets
                setShowAdd(false)
                setProductName(defaultName)
                setProductDescription(defaultDescription)
                setProductPicture(defaultPicture)
                setProductQty(defaultQty)
                setProductPrice(defaultPrice)
                setIsActiveAdd(false)
            }

            console.log(data);
        })
    }


    return (
        (!user.isAdmin) ? <Navigate to="/products"/>
        :
        <>
            <div className='custom-bg-white'>
                <Container className="custom-account-wrapper py-5">

                    <div className='custom-title-section mb-5'>
                        <h1 className='custom-page-title'>All Products.</h1>

                        <Button variant="primary" className="custom-button-primary rounded-0 custom-add-button" onClick={() => setShowAdd(true)}>Add Product</Button>

                    </div>
                    
                    <div className='custom-products-section'>
                        <div className='custom-products-wrapper'>
                            {
                                isFetched
                                    ?   <>{products}</> 
                                    :   <div>
                                            <h2 className="custom-page-sub-title mb-3">Defining perfection...</h2>
                                        </div>
                            }
                        </div>
                    </div>
                </Container>            
            </div>

            {/* Add Product Full Screen - Modal  */}
            <Modal
                show={showAdd}
                fullscreen={true}
                onHide={() => {
                    setShowAdd(false)
                    setProductName(defaultName)
                    setProductDescription(defaultDescription)
                    setProductPicture(defaultPicture)
                    setProductQty(defaultQty)
                    setProductPrice(defaultPrice)
                }}
                >
                <Modal.Header closeButton>
                    <Modal.Title className="custom-brand-logo p-2">
                        PTRCK<span className="mx-1 custom-brand-r">®</span>
                        Product
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="custom-modal-add-body">
                    <div className="custom-modal-prod-view border bg-white">

                            {/* Product Picture */}
                            <div className="picture-section">
                                <img src={productPicture} alt="New Product Picture"/>
                            </div>
                            {/* <div className='text-section'>
                                details here
                            </div> */}

                            <div className="custom-section-text">
                                <small className="text-muted custom-section-small">PTRCK Apparel</small>
                                <h2 className="custom-section-header mt-3 mb-4">{productName}</h2>
                                <p className='custom-section-p'>{productDescription}</p>
                                <small className='custom-section-small'>Disclaimer: Product color may slightly vary due to photographic lighting sources or your monitor settings.</small>
                                <h3 className="custom-product-price my-4">PHP {productPrice}</h3>
                                <small className='custom-section-small'>Stocks: {productQty}</small>
                                {/* <Button as={Link} to="/login" variant="primary" className="custom-button-primary custom-add-to-cart mt-4">Add to cart</Button> */}
                            </div>
                    </div>

                    <Form className="custom-modal-form border bg-white" onSubmit={(e) => addProduct(e)}>
                        <h2 className="custom-page-title my-4">Add Product.</h2>
                        {/* Product Name */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                className="custom-input"
                                type="text"
                                placeholder="PTRCK New Product"
                                onChange={e => setProductName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Product Description */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Description</Form.Label>
                            <textarea
                                className="form-control custom-input"
                                id="exampleFormControlTextarea1"
                                rows="3"
                                placeholder='Perfect description for a perfect product'
                                onChange={e => setProductDescription(e.target.value)}
                                required>
                            </textarea>
                        </Form.Group>

                        {/* Product Quantity */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Quantity</Form.Label>
                            <Form.Control 
                                className="custom-input"
                                type="number"
                                placeholder="Enter stocks quantity"
                                onChange={e => setProductQty(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Product Price */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                                className="custom-input"
                                type="number"
                                placeholder="Enter product price"
                                onChange={e => setProductPrice(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Product Picture */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Picture Link</Form.Label>
                            <Form.Control
                                className="custom-input"
                                type="text"
                                placeholder="https://ptrckco.com"
                                onChange={e => setProductPicture(e.target.value)}
                                required    
                            />
                        </Form.Group>

                        {
                            isActiveAdd
                                ?   <button className="custom-button-add-prod mt-1" type="submit">Add</button>
                                :   <button className="custom-button-add-prod-disabled  mt-1" type="submit" disabled>Add</button>
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    );

};


