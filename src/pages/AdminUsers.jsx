import { useState, useEffect } from 'react';
import {Container, Table} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminUsers() {

    const [users, setUsers] = useState([]);
    const [reload, setReload] = useState(false);

    useEffect(() => {
        fetch('https://frozen-beach-99494.herokuapp.com/users/all', {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUsers(
                data.map(user => {
                    return (
                        <tr key={user._id} className={user.isAdmin ? 'deliver-success' : 'bg-light'}>
                            <td>{user._id}</td>
                            <td>{user.lastName}</td>
                            <td>{user.firstName}</td>
                            <td>{user.email}</td>
                            <td>{user.isAdmin ? 'Admin' : 'Regular User'}</td>
                            <td>
                                {
                                    user.isAdmin
                                        ?   <button className='custom-deliver-disabled'disabled>Make Admin</button>
                                        :   <button className='custom-button-primary custom-deliver-button' onClick={()=>makeAdmin(user._id)}>Make Admin</button>
                                }
                            </td>
                        </tr>
                    )
                })
            )
        })

    }, [reload])

    const makeAdmin = (id) => {
        fetch(`https://frozen-beach-99494.herokuapp.com/users/${id}/setAsAdmin`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                Swal.fire({
                    title: 'User is now an Admin',
                    icon: 'success',
                });
                setReload(!reload);
            }
            else {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error',
                });
            }
        })
    }

    return (
        <>
            <div className=''>
                <Container className="custom-account-wrapper py-5">
                    <h1 className='custom-page-title mb-5'>All Users.</h1>
                    <div className='custom-account-section'>
                        <Table hover>
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {users}
                            </tbody>
                        </Table>
                    </div>
                </Container>
            </div>
        </>
    );

};