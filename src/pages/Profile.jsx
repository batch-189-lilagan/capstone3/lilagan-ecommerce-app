import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';
import AccountCard from '../components/AccountCard';
import AccountOrders from '../components/AccountOrders'


export default function Profile() {

    const { user } = useContext(UserContext);

    const [orders, setOrders] = useState([]);

    // Retieve User Orders 
    const fetchUserOrder = (token) => {
		fetch('https://frozen-beach-99494.herokuapp.com/users/myOrders', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
            if (data === false) {
                data = [];
            }
			setOrders(data)
		});
	};

    // Get user details when this page loads
    useEffect(() => {
        fetchUserOrder(localStorage.getItem('token'))
    }, []);

    return (
        <>
            <div className=''>
                <Container className="custom-account-wrapper py-5">
                    <h1 className='custom-page-title mb-5'>Account.</h1>
                    <div className='custom-account-section'>
                        <AccountCard user={ user }/>
                        <AccountOrders orders={ orders }/>
                    </div>
                </Container>
            </div>
        </>
    );

};