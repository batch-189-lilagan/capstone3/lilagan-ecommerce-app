import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Container, Form } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom'

export default function Login() {

    // Unpack here the userContext
    const { user, setUser } = useContext(UserContext);

    // State hooks for the inputs
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


    // useEffect to monitor the input field
    useEffect(() => {
        if (email !== '' && password !=='') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);


    // Authenticate Button Function
    function authenticate(e) {
        e.preventDefault();

        fetch('https://frozen-beach-99494.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,
                password
            })
        })
        .then(res => res.json())
        .then(data => {
            
            // If there is a server error
            if (data.error === 'serverError') {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'Error',
                    text: 'A server error occured when trying to make the request. Please try again.'
                })
            }

            // If email doesn't exist
            else if (data.error === 'emailNotFound') {
                Swal.fire({
                    title: "Email not yet registered",
                    icon: "warning",
                    text: "Check your email, and try again."
                })
            }

            // If password is incorrect
            else if (data.error === 'incPassword') {
                Swal.fire({
                    title: "Incorrect password",
                    icon: "warning",
                    text: "Check your password, and try again."
                })
            }

            // If success
            else {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: 'Signed in successfully!',
                    icon: 'success',
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 6000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                // Reset the email and password states after login
                setEmail("");
                setPassword("");
            }
        });
    };

    // Retieve User Details for the authenticate function
    const retrieveUserDetails = (token) => {
		fetch('https://frozen-beach-99494.herokuapp.com/users/profile', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email,
				isAdmin: data.isAdmin
			});
		});
	};



    return (
        // Redirect to Home if user tried to access /login
        (user.id !== null) ? <Navigate to="/"/>
        :
        <>
            <div className="custom-login-bg">
                <Container className="custom-login-wrapper">
                    <Form className="custom-form border" onSubmit={(e) => authenticate(e)}>
                        <h2 className="custom-page-title mb-4">Login.</h2>

                        {/* Email */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                className="custom-input"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        {/* Password */}
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                className="custom-input"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* If email and password contains a value, enable the button*/}
                        {
                            isActive
                                ? <button className="custom-button-primary mt-1" type="submit">Log In</button>
                                : <button className="custom-button-disabled mt-1" type="submit" disabled={true}>Log In</button>
                        }
                        
                        <Form.Text className="text-muted mt-3">
                            Don't have an account yet?&nbsp;
                            <Link to="/register" className="custom-register-here">
                                Register here.
                            </Link>
                        </Form.Text>
                    </Form>
                </Container>
            </div>
        </>
    );

};