import { useState, useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { Container, Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2';

export default function Cart() {

    /*
        - user id from the local storage
        - get the user cart

        - check out button
    */

    const [cart, setCart] = useState([])
    const [total, setTotal] = useState(0);

    useEffect(() => {
        fetch('https://frozen-beach-99494.herokuapp.com/users/myCart', {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data === []) {
                // Empty Cart
                setCart([])
            }

            else {
                let total = 0;
                
                setCart(
                    data.map((product) => {

                        total += product.price;

                        return (
                            <tr key={product._id}>
                                <td className="py-3">{product.productName}</td>
                                <td className="py-3">PHP {product.price}</td>
                            </tr>
                        )
                    })
                )

                setTotal(total);
            }
        })
    }, [])


    const placeOrder = () => {
        fetch('https://frozen-beach-99494.herokuapp.com/users/checkout', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data) {
                Swal.fire({
                    title: 'We are now processing your order.',
                    icon: 'success',
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 6000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                setCart([]);
            }
            else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Try again later."
                })
            }
        })

    }

    return (
        <>
            <div className=''>
                <Container className="custom-account-wrapper py-5">
                    <h1 className='custom-page-title mb-5'>Cart.</h1>
                    {
                        cart.length === 0
                            ?   <>  
                                    <p className="custom-account-details">No products in cart yet.</p>
                                    <Button as={Link} to="/products" variant="primary" className="custom-button-primary custom-add-to-cart custom-continue-shop mt-4">Continue Shopping</Button>
                                </>
                            :   <><div className='custom-account-section'>
                                <Table hover>
                                    <thead>
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Subtotal</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        {cart}
                                    </tbody>
                                </Table>
                            </div>
                            <div className="custom-button-part mt-5">
                                <small className="text-muted custom-section-small">Total</small>
                                <p className="custom-cart-total">PHP {total}</p>
                                {/* <small className="text-muted custom-section-small">You are now on your way to greatness.</small> */}
        
                                <div className="mt-1 custom-button-group">
                                    <Button variant="primary" className="custom-button-primary custom-add-to-cart" onClick={() => placeOrder()}>Place order</Button>
        
                                    <Button as={Link} to="/products" variant="primary" className="custom-button-primary custom-add-to-cart custom-continue-shop">Continue Shopping</Button>
                                </div>
                            </div></>
                    }         
                </Container>
            </div>
        </>
    );

};