import { useContext } from 'react';
import UserContext from '../UserContext';
import { Container } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function Footer() {

    return (
        <>
            <div className="custom-footer">
                <p className="mt-3">PTRCK® 2022 ALL RIGHTS RESERVED TO CHARLES PATRICK LILAGAN</p>
            </div>
        </>
        
    );

};