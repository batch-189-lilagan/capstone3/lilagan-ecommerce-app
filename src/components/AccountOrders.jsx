import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";

export default function AccountOrders({orders}) {

    const [ordersRow, setOrdersRow] = useState([]);
    const [hasOrders, setHasOrders] = useState(false);

    useEffect(() => {

        if (orders.length > 0) {
            const tmpOrders = orders.map(order => {

                let status = order.isDelivered ? 'Delivered' : 'Pending';
                
                let date = new Date(order.placedOn).toDateString();
    
                return (
                    <tr key={order._id}>
                        <td className="py-4">PTRCK #{order._id}</td>
                        <td className="py-4">{date}</td>
                        <td className="py-4">{status}</td>
                        <td className="py-4">{order.total}</td>
                    </tr>
                )
            })
            setOrdersRow(tmpOrders);
            setHasOrders(true)
        }   
        
    }, [orders])

    return (
        <>
            <div className="custom-order-wrapper">
                <h2 className="custom-page-sub-title mb-3">Order History</h2>

                {
                    hasOrders
                        ?   <Table hover>
                                <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Total</th>
                                        </tr>
                                </thead>
                                <tbody>
                                    {ordersRow}
                                    {/* <tr>
                                        <td>#PTRCK-01</td>
                                        <td>12/11/19</td>
                                        <td>Pending</td>
                                        <td>PHP 1,250</td>
                                    </tr>
                                    <tr>
                                        <td>#PTRCK-02</td>
                                        <td>12/11/19</td>
                                        <td>Pending</td>
                                        <td>PHP 2,500</td>
                                    </tr>
                                    <tr>
                                        <td>#PTRCK-03</td>
                                        <td>12/11/19</td>
                                        <td>Pending</td>
                                        <td>PHP 1,500</td>
                                    </tr> */}
                                </tbody>
                            </Table>
                        : <p className="custom-account-details">You haven't placed an order yet.</p>

                }
            </div>
        </>
    );

};