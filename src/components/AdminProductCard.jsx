import { useEffect, useState } from 'react';
import {Card, Button, Badge, Modal, Form} from 'react-bootstrap';
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2';

export default function AdminProductCard({productData, getProducts}) {

    let data = {
        id: productData._id,
        name: productData.name,
        description: productData.description,
        price: productData.price,
        picture: productData.picture,
        isActive: productData.isActive,
        stocks: productData.stocks
    }

    // Edit Product Form useStates
    const [productName, setProductName] = useState(data.name);
    const [productDescription, setProductDescription] = useState(data.description);
    const [productPicture, setProductPicture] = useState(data.picture);
    const [productQty, setProductQty] = useState(data.stocks);
    const [productPrice, setProductPrice] = useState(data.price);

    const [isActiveSave, setIsActiveSave] = useState(false);

    // Archive Product Function
    const archiveProduct = (id) => {

        fetch(`https://frozen-beach-99494.herokuapp.com/products/${id}/archive`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(resData => {
            if (resData.error === 'serverError') {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error'
                });
            }

            else if (resData === true) {

                Swal.fire({
                    title: 'Product Archived',
                    icon: 'success',
                });

                getProducts();
            }

            else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error'
                });
            }
        })
    }

    // Activate Product Function
    const activateProduct = (id) => {

        fetch(`https://frozen-beach-99494.herokuapp.com/products/${id}/enable`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(resData => {
            if (resData.error === 'serverError') {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error'
                });
            }

            else if (resData === true) {

                Swal.fire({
                    title: 'Product Activated',
                    icon: 'success',
                });

                getProducts();
            }

            else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error'
                });
            }
        })
    }

    const [showEdit, setShowEdit] = useState(false);

    const handleClose = () => setShowEdit(false);
    const handleShow = () => setShowEdit(true);

    useEffect(() => {

        let noEmpty = (
            (productName !== '') &&
            (productDescription !== '') &&
            (productQty !== '') &&
            (productPicture !== '') &&
            (productPrice !== '')
        )

        let isChanged = (
            (productName !== data.name) ||
            (productDescription !== data.description) ||
            (productQty.toString() !== data.stocks.toString()) ||
            (productPrice.toString() !== data.price.toString()) ||
            (productPicture !== data.picture)
        )

        let validInput = noEmpty && isChanged;

        if (validInput) {
            setIsActiveSave(true)
        }

        else {
            setIsActiveSave(false)
        }

    }, [productName, productDescription, productPicture, productQty, productPrice]);


    // Save edit product submit function 
    const saveChanges = (e) => {

        e.preventDefault();

        fetch(`https://frozen-beach-99494.herokuapp.com/products/${data.id}/update`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: productName,
                description: productDescription,
                picture: productPicture,
                price: parseInt(productPrice),
                stocks: parseInt(productQty)
            })
        })
        .then(res => res.json())
        .then(data => {
            
            if (data) {
                Swal.fire({
                    title: 'Product Updated',
                    icon: 'success',
                    text: 'Users will now be able to avail this new perfection.'
                });     
                
                // useState resets
                setShowEdit(false)
                setIsActiveSave(false)
                getProducts();
            }

            else {
                Swal.fire({
                    title: 'Server Error',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }
        })
    }



    return (
        <>
            <Card className="custom-card rounded-0">
                <Card.Img className="rounded-0"variant="top" src={data.picture} />
                <Card.Body>
                    <Card.Title className="custom-product-link" as={ Link } to={`/products/${data.id}`}>
                        {data.name}
                        {
                            data.isActive
                            ?   <Badge bg="success" className="custom-badge">Active</Badge>
                            :   <Badge bg="danger" className="custom-badge">Archived</Badge>
                        }
                    </Card.Title>
                    <Card.Text>
                        PHP {data.price}
                    </Card.Text>
                    <Card.Text>
                        Stocks: {data.stocks}
                    </Card.Text>
                    <Card.Text>
                    </Card.Text>
                </Card.Body>
                <Button variant="primary" className="custom-button-primary mx-3 mb-2 rounded-0" onClick={() => handleShow()}>Edit</Button>
                {
                    data.isActive
                    ?   <Button variant="primary" className="custom-button-primary custom-continue-shop custom-archive-primary mx-3 mb-3 rounded-0" onClick={() => archiveProduct(data.id)}>Archive</Button>

                    :   data.stocks !== 0
                        ?   <Button  Button variant="primary" className="custom-button-primary custom-continue-shop custom-enable-primary mx-3 mb-3 rounded-0" onClick={() => activateProduct(data.id)}>Activate</Button>
                        :   <Button variant="primary" className="custom-button-primary custom-continue-shop disable mx-3 mb-3 rounded-0" onClick={() => activateProduct(data.id)} disabled>No Stocks Available</Button>
                }   
            </Card>

            {/* Edit Model */}
            <Modal
                show={showEdit}
                fullscreen={true}
                onHide={handleClose}
                >
                <Modal.Header closeButton>
                    <Modal.Title className="custom-brand-logo p-2">
                        PTRCK<span className="mx-1 custom-brand-r">®</span>
                        Product
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="custom-modal-add-body">
                    <div className="custom-modal-prod-view border bg-white">

                            {/* Product Picture */}
                            <div className="picture-section">
                                <img src={productPicture} alt="New Product Picture"/>
                            </div>

                            <div className="custom-section-text">
                                <small className="text-muted custom-section-small">PTRCK Apparel</small>
                                <h2 className="custom-section-header mt-3 mb-4">{productName}</h2>
                                <p className='custom-section-p'>{productDescription}</p>
                                <small className='custom-section-small'>Disclaimer: Product color may slightly vary due to photographic lighting sources or your monitor settings.</small>
                                <h3 className="custom-product-price my-4">PHP {productPrice}</h3>
                                <small className='custom-section-small'>Stocks: {productQty}</small>
                                {/* <Button as={Link} to="/login" variant="primary" className="custom-button-primary custom-add-to-cart mt-4">Add to cart</Button> */}
                            </div>
                    </div>

                    <Form className="custom-modal-form border bg-white" onSubmit={(e) => saveChanges(e)}>
                        <h2 className="custom-page-title my-4">Edit Product.</h2>
                        {/* Product Name */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                className="custom-input"
                                type="text"
                                placeholder="PTRCK New Product"
                                value={productName}
                                onChange={e => setProductName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Product Description */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Description</Form.Label>
                            <textarea
                                className="form-control custom-input"
                                id="exampleFormControlTextarea1"
                                rows="3"
                                placeholder='Perfect description for a perfect product'
                                value={productDescription}
                                onChange={e => setProductDescription(e.target.value)}
                                required>
                            </textarea>
                        </Form.Group>

                        {/* Product Quantity */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Quantity</Form.Label>
                            <Form.Control 
                                className="custom-input"
                                type="number"
                                placeholder="Enter stocks quantity"
                                value={productQty}
                                onChange={e => setProductQty(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Product Price */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                                className="custom-input"
                                type="number"
                                placeholder="Enter product price"
                                value={productPrice}
                                onChange={e => setProductPrice(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {/* Product Picture */}
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Picture Link</Form.Label>
                            <Form.Control
                                className="custom-input"
                                type="text"
                                placeholder="https://ptrckco.com"
                                value={productPicture}
                                onChange={e => setProductPicture(e.target.value)}
                                required    
                            />
                        </Form.Group>

                        {
                            isActiveSave
                                ?   <button className="custom-button-add-prod mt-1" type="submit">Save</button>
                                :   <button className="custom-button-add-prod-disabled  mt-1" type="submit" disabled>Save</button>
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    );

};


