import {useState, useContext} from 'react';
import UserContext from '../UserContext';
import {Card, Button, Badge} from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom'
import Swal from 'sweetalert2';

export default function ProductCard({productData}) {

    const { user } = useContext(UserContext);

    let data = {
        id: productData._id,
        name: productData.name,
        price: productData.price,
        picture: productData.picture,
    }

    const addToCart = (data) => {
        
        fetch(`https://frozen-beach-99494.herokuapp.com/users/addToCart`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
            body: JSON.stringify({
                productId: data.id,
                productName: data.name,
                quantity: 1,
                price: data.price
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: 'Added to cart!',
                    icon: 'success',
                    toast: true,
                    position: 'center',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
            }

            else {
                Swal.fire({
                    title: "Server Error",
                    icon: "error",
                })
            }
        })
    }

    return (
        <>
            <Card className="custom-card rounded-0">
                <Card.Img className="rounded-0"variant="top" src={data.picture} />
                <Card.Body>
                    <Card.Title className="custom-product-link" as={ Link } to={`/products/${data.id}`}>
                        {data.name}
                    </Card.Title>
                    <Card.Text>
                        PHP {data.price}
                    </Card.Text>
                    <Card.Text>
                    </Card.Text>
                </Card.Body>
                {
                    user.id !== null
                        ?   <Button variant="primary" className="custom-button-primary m-3 rounded-0" onClick={() => addToCart(data)}>Add to cart</Button>
                        :   <Button as={Link} to="/login" variant="primary" className="custom-button-primary m-3 rounded-0">Add to cart</Button>
                }
                
            </Card>
        </>
    );

};