export default function AccountCard({user}) {

    const fullName = `${user.firstName} ${user.lastName}`;
    const email = user.email;

    // console.log(user);

    const hasFetchedData = user.firstName ? true : false;

    return (
        <>
            <div>
                <h2 className="custom-page-sub-title mb-3">Details</h2>
                {
                    hasFetchedData
                        ?   <><p className="custom-account-details">{fullName}</p>
                            <p className="custom-account-details">{email}</p></>
                        :   <p className="custom-account-details">Fetching data...</p>
                }
                

                <div className="custom-line mt-3 mb-4"></div>
                <em className="text-muted">This user has discovered perfection.</em>
            </div>
        </>
    );

};