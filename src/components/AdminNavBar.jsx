import { useContext } from 'react';
import UserContext from '../UserContext';
import {Container, Nav, Navbar} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function NavBar() {

    const { user } = useContext(UserContext);

    return (
        <>
        <Navbar className="p-2 custom-navbar" sticky="top">
            <Container>
                <Navbar.Brand as={Link} to="/" className="custom-brand-logo">PTRCK<span className="mx-1 custom-brand-r">®</span> Admin Panel</Navbar.Brand>
                <Nav className="d-flex align-items-center">
                    <Nav.Link as={Link} to="/products/all" className="mx-3 custom-navbar-links">PRODUCTS</Nav.Link>
                    <Nav.Link as={Link} to="/orders" className="mx-3 custom-navbar-links">ORDERS</Nav.Link>
                    <Nav.Link as={Link} to="/allUsers" className="mx-3 custom-navbar-links">USERS</Nav.Link>
                    {
                        (user.id !== null)
                            ?   <>
                                    <Nav.Link as={Link} to="/logout" className="mx-3 px-4 custom-button">LOG OUT</Nav.Link>
                                </>
                            
                            :   <Nav.Link as={Link} to="/login" className="mx-3 px-4 custom-button">LOG IN</Nav.Link>
                    }
                </Nav>
            </Container>
        </Navbar>
        </>
    );

};