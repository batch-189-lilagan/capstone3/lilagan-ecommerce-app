import { useContext } from 'react';
import UserContext from '../UserContext';
import { Container } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function Hero() {

    const { user } = useContext(UserContext);

    return (
        <>
        {

            user.isAdmin
            // If the one logged in is an admin...
                ?   <div className="custom-admin-panel-hero">
                        <Container className="custom-hero-wrapper">
                            <div className="custom-hero-banner">
                                {/* <h1 className="custom-brand-logo">PTRCK</h1> */}
                                <h2>Welcome back to your Admin Panel</h2>
                                <p>The world deserves perfection. Start releasing products and have them taste the true perfection.</p>
                                {/* <button as={Link} to="/products" className="custom-shop-now">SHOP NOW</button> */}
                                <Link to="/products/all" className="custom-shop-now">MANAGE PRODUCTS</Link>
                            </div>

                            {/* <div className="custom-hero-img">
                                IMG HERE
                            </div> */}
                        </Container>
                    </div>
            // If just a user or non user
                :   <div className="custom-hero-bg">
                    <Container className="custom-hero-wrapper">
                        <div className="custom-hero-banner">
                            {/* <h1 className="custom-brand-logo">PTRCK</h1> */}
                            <h2>Life isn’t perfect but your streetwear can be.</h2>
                            <p>Don't know if "perfect" really do exist? Browse our products and see it for yourself.</p>
                            {/* <button as={Link} to="/products" className="custom-shop-now">SHOP NOW</button> */}
                            <Link to="/products" className="custom-shop-now">SHOP NOW</Link>
                        </div>

                        {/* <div className="custom-hero-img">
                            IMG HERE
                        </div> */}
                    </Container>
                </div>
        }
            
        </>
        
    );

};