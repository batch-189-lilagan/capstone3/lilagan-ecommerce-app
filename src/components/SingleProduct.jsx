import { useContext } from 'react';
import UserContext from '../UserContext';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2';


export default function SingleProduct({productData}) {

    const { user } = useContext(UserContext);

    let product = {
        id: productData._id,
        name: productData.name,
        description: productData.description,
        price: productData.price,
        picture: productData.picture
    }

    const addToCart = (data) => {
        
        fetch(`https://frozen-beach-99494.herokuapp.com/users/addToCart`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
            body: JSON.stringify({
                productId: product.id,
                productName: product.name,
                quantity: 1,
                price: product.price
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: 'Added to cart!',
                    icon: 'success',
                    toast: true,
                    position: 'center',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
            }

            else {
                Swal.fire({
                    title: "Server Error",
                    icon: "error",
                })
            }
        })
    }

    return (
        <div className="custom-single-product border">
            <div className="custom-section-picture">
                <img src={product.picture}/>
            </div>

            <div className="custom-section-text">
                <small className="text-muted custom-section-small">PTRCK Apparel</small>
                <h2 className="custom-section-header mt-3 mb-4">{product.name}</h2>
                <p className='custom-section-p'>{product.description}</p>
                <small className='custom-section-small'>Disclaimer: Product color may slightly vary due to photographic lighting sources or your monitor settings.</small>
                <h3 className="custom-product-price my-4">PHP {product.price}</h3>
                {
                    user.id !== null
                        ?   <Button variant="primary" className="custom-button-primary custom-add-to-cart" onClick={() => addToCart(product)}>Add to cart</Button>
                        :   <Button as={Link} to="/login" variant="primary" className="custom-button-primary custom-add-to-cart">Add to cart</Button>
                }
            </div>
        </div>
    );

};